extern crate nom;
extern crate nom_greedyerror;
extern crate nom_locate;

use crate::parser::{tokentag, ParseResult};
use crate::tokens::{Tokens, TokenKind};

use nom::branch::alt;
use nom::multi::{separated_nonempty_list, many1};
use nom::combinator::{opt, recognize};
use nom::sequence::{pair, delimited, preceded, separated_pair, terminated, tuple};

pub fn quantity(input: Tokens) -> ParseResult<Tokens> {
    recognize(many1(
        pair(
            alt((tokentag(TokenKind::IntConstant), realconstant_ext)),
            tokentag(TokenKind::Name)),
    ))(input)
}

pub fn realconstant_ext(input: Tokens) -> ParseResult<Tokens> {
    alt((tokentag(TokenKind::RealConstant), tokentag(TokenKind::Inf), tokentag(TokenKind::Nan)))(input)
}

pub fn numliteral(input: Tokens) -> ParseResult<Tokens> {
    alt((quantity, tokentag(TokenKind::IntConstant), realconstant_ext))(input)
}

pub fn literal(input: Tokens) -> ParseResult<Tokens> {
    alt((
        tokentag(TokenKind::StringConstant),                // stringliteral
        alt((tokentag(TokenKind::True), tokentag(TokenKind::False))), // boolliteral
        numliteral,
    ))(input)
}

pub fn simple_expr(input: Tokens) -> ParseResult<Tokens> {
    alt((identifier, operator, literal))(input)
}


pub fn identifier(input: Tokens) -> ParseResult<Tokens> {
    alt((
        recognize(separated_pair(tokentag(TokenKind::This), tokentag(TokenKind::Period), tokentag(TokenKind::Name))),
        recognize(separated_pair(tokentag(TokenKind::Name), tokentag(TokenKind::Period), tokentag(TokenKind::Name))),
        recognize(separated_pair(
            terminated(tokentag(TokenKind::Name), delimited(tokentag(TokenKind::SquareLeft), expr_parens, tokentag(TokenKind::SquareRight))),
            tokentag(TokenKind::Period),
            tokentag(TokenKind::Name),
        )),
        tokentag(TokenKind::Name),
    ))(input)
}

pub fn operator(input: Tokens) -> ParseResult<Tokens> {
    alt((
        recognize(pair(tokentag(TokenKind::Index), pair(tokentag(TokenKind::ParenLeft), tokentag(TokenKind::ParenRight)))),
        recognize(pair(
            tokentag(TokenKind::Exists),
            delimited(tokentag(TokenKind::ParenLeft), identifier, tokentag(TokenKind::ParenRight)),
        )),
        recognize(pair(
            tokentag(TokenKind::Sizeof),
            delimited(tokentag(TokenKind::ParenLeft), identifier, tokentag(TokenKind::ParenRight)),
        )),
        tokentag(TokenKind::Index),
        tokentag(TokenKind::Typename),
    ))(input)
}

fn funcname(input: Tokens) -> ParseResult<Tokens> {
    alt((tokentag(TokenKind::XmlDoc), tokentag(TokenKind::Xml), tokentag(TokenKind::Name)))(input)
}


fn convname(input: Tokens) -> ParseResult<Tokens> {
    alt((tokentag(TokenKind::Int), tokentag(TokenKind::Double), tokentag(TokenKind::String)))(input)
}

/*
fn expr_func(input: Tokens) -> ParseResult<Tokens> {
    recognize(pair(
        funcname,
        delimited(tokentag(TokenKind::ParenLeft), separated_list(tokentag(TokenKind::Comma), expr_parens), tokentag(TokenKind::ParenRight))
    ))(input)
}
*/

/*
fn expr_conv(input: Tokens) -> ParseResult<Tokens> {
    recognize(pair(
        alt((tokentag(TokenKind::Int), tokentag(TokenKind::Double), tokentag(TokenKind::String))),
        delimited(tokentag(TokenKind::ParenLeft), separated_list(tokentag(TokenKind::Comma), expr_parens), tokentag(TokenKind::ParenRight))
    ))(input)
}
*/

fn expr_inside(input: Tokens) -> ParseResult<Tokens> {
    recognize(
        many1(
            alt((
                tokentag(TokenKind::Comma),
                expression,
                recognize(delimited(
                    tokentag(TokenKind::ParenLeft),
                    expression,
                    tokentag(TokenKind::ParenRight),
                )),
            )
        ),
    ))(input)
}
fn expr_part(input: Tokens) -> ParseResult<Tokens> {
    alt((
        preceded(
            alt((
                convname,
                funcname
            )),
            delimited(
                tokentag(TokenKind::ParenLeft),
                expr_inside,
                tokentag(TokenKind::ParenRight),
            )
        ),
        simple_expr
    ))(input)
}

fn expr_parens(input: Tokens) -> ParseResult<Tokens> {
    recognize(
        alt((
            expr_part, // TODO
            recognize(delimited(
                tokentag(TokenKind::ParenLeft),
                expr_inside,
                tokentag(TokenKind::ParenRight),
            )),
        ))
    )(input)
}

macro_rules! bin_op {
    ($b:ident, $n:ident, [ $tk:expr ]) => {
        pub fn $n(input: Tokens) -> ParseResult<Tokens> {
            recognize(
                separated_nonempty_list(
                    tokentag($tk),
                    $b
                )
            )(input)
        }
    };

    ($b:ident, $n:ident, [ $( $tk:expr ),* ]) => {
        pub fn $n(input: Tokens) -> ParseResult<Tokens> {
            recognize(
                separated_nonempty_list(
                    alt((
                        $( tokentag($tk) ),*
                    )),
                    $b
                )
            )(input)
        }
    }
}

// The macro that expands into all pairs
macro_rules! for_all_pairs {
    ($mac:ident: [$($x:tt),*]) => {
        // Duplicate the list
        for_all_pairs!(@inner $mac: $($x)*; $($x)*);
    };

    // The end of iteration: we exhausted the list
    (@inner $mac:ident: ; $($x:tt)*) => {};

    // The head/tail recursion: pick the first element of the first list
    // and recursively do it for the tail.
    (@inner $mac:ident: $head:ident $($tail:ident)*; $($x:ident)*) => {
        $(
            $mac!($head, $x);
        )*
        for_all_pairs!(@inner $mac: $($tail)*; $($x)*);
    };
}

/*
macro_rules! bin_ops {
    ($b:ident, [ $( ( $level:ident, $tks:tt ) ),* ] ) => {
        $(
            bin_op!( $level, $level, $tks  );
        )*
    }
}

bin_ops!(simple_expr, [
    (logical, [TokenKind::Or, TokenKind::Xor]),
    (less, [TokenKind::Or, TokenKind::Xor]),
    (greater, [TokenKind::Or, TokenKind::Xor])
]);
*/

fn expr_unary(input: Tokens) -> ParseResult<Tokens> {
    recognize(
        preceded(
            opt(
                alt((
                    tokentag(TokenKind::MinusSign),
                    tokentag(TokenKind::ExclamationPoint),
                    tokentag(TokenKind::Tilde),
                ))
            ),
            expr_parens
        )
    )(input)
}


bin_op!(expr_unary, expr_powerof, [TokenKind::Hat]);
bin_op!(expr_powerof, expr_muldiv, [TokenKind::Asterisk, TokenKind::Slash, TokenKind::PercentSign]);
bin_op!(expr_muldiv, expr_addsub, [TokenKind::PlusSign, TokenKind::MinusSign]);
bin_op!(expr_addsub, expr_bitwise_shift, [TokenKind::ShiftLeft, TokenKind::ShiftRight]);
bin_op!(expr_bitwise_shift, expr_bitwise_logical, [TokenKind::Ampersand, TokenKind::Pipe, TokenKind::Hashmark]);
bin_op!(expr_bitwise_logical, expr_equal, [TokenKind::Eq]);
bin_op!(expr_equal, expr_notequal, [TokenKind::Ne]);
bin_op!(expr_notequal, expr_greater, [TokenKind::PointyRight, TokenKind::Ge]);
bin_op!(expr_greater, expr_less, [TokenKind::PointyLeft, TokenKind::Le]);
bin_op!(expr_less, expr_logical, [TokenKind::And, TokenKind::Or, TokenKind::Xor]);

/*
macro_rules! noop {
    ($b:ident, $c:ident) => {

    }
}
for_all_pairs!(noop: [e, d] );
*/

pub fn expr_ternary(input: Tokens) -> ParseResult<Tokens> {
    recognize(
        alt((
            recognize(tuple((
                expr_logical,
                tokentag(TokenKind::Questionmark),
                expr_logical,
                tokentag(TokenKind::Colon),
                expr_ternary
            ))),
        expr_logical
    )))(input)
}

pub fn expression(input: Tokens) -> ParseResult<Tokens> {
    expr_ternary(input)
}

/*
+--------------+---------------------------------------------+
|  Operator    |  Meaning                                    |
+--------------+---------------------------------------------+
|  -, !, ~     |  unary minus, negation, bitwise complement  |
|  ^           |  power-of                                   |
|  *, /, %     |  multiply, divide, integer modulo           |
|  +, -        |  add, subtract, string concatenation        |
|  <<, >>      |  bitwise shift                              |
|  &, |, #     |  bitwise and, or, xor                       |
|  ==          |  equal                                      |
|  !=          |  not equal                                  |
|  >, >=       |  greater than, greater than or equal to     |
|  <, <=       |  less than, less than or equal to           |
|  &&, ||, ##  |  logical operators and, or, xor             |
|  ?:          |  the C/C++ “inline if”                      |
+--------------+---------------------------------------------+
*/


/*
#[test]
fn test_expr2() {

    assert!(expr2(Span::new("aaa")).is_ok());
    assert!(expr2(Span::new("this.aaa")).is_ok());
    assert!(expr2(Span::new("aaa.bbb")).is_ok());
    assert!(expr2(Span::new("aaa[eee].bbb")).is_ok());

    assert!(expr2(Span::new("index")).is_ok());
    assert!(expr2(Span::new("typename")).is_ok());

    // TODO: check for it not accepting keywords
}
*/
