use wasm_bindgen::prelude::*;

use crate::*;
use tokens::{Tokens, TokenKind};
use crate::parser::nedfile;


#[wasm_bindgen]
#[derive(Copy, Clone, Debug)]
pub struct TokenLoc {
    kind: TokenKind,
    start_line: u32,
    start_offset: usize,
    length: usize
}

#[wasm_bindgen]
impl TokenLoc {
    pub fn get_kind_str(&self) -> String {
        format!("{:#?}", self.kind)
    }

    pub fn get_start_line(&self) -> u32 {
        self.start_line
    }

    pub fn get_start_offset(&self) -> usize {
        self.start_offset
    }

    pub fn get_length(&self) -> usize {
        self.length
    }
}



#[wasm_bindgen]
#[derive(Debug, Default)]
pub struct ParseResultForJs {
    toks: Vec<TokenLoc>,
    tokens: String,
    result: String,
    error: String,
}

impl ParseResultForJs {
    pub fn map_toks(ts: Tokens) -> Vec<TokenLoc> {
        ts.tok.iter().map(|c| TokenLoc { kind: c.kind, start_line: c.content.location_line(), start_offset: c.content.location_offset(), length: c.content.fragment().len() }).collect::<Vec<TokenLoc>>()
    }

    pub fn parse_error(ts: Tokens, tok: String, err: String) -> ParseResultForJs {
        ParseResultForJs { toks: ParseResultForJs::map_toks(ts), tokens: tok, result: String::new(), error: err }
    }

    pub fn success(ts: Tokens, tok: String, res: String) -> ParseResultForJs {
        ParseResultForJs {toks: ParseResultForJs::map_toks(ts), tokens: tok, result: res, error: String::new() }
    }

    pub fn error(err: String) -> ParseResultForJs {
        ParseResultForJs { toks: vec![], tokens: String::new(), result: String::new(), error: err }
    }

}

#[wasm_bindgen]
impl ParseResultForJs {

    pub fn get_num_toks(&self) -> usize {
        self.toks.len()
    }

    pub fn get_tok_loc(&self, i: i32) -> TokenLoc {
        *self.toks.get(i as usize).unwrap()
    }

    pub fn get_tokens(&self) -> String {
        self.tokens.clone()
    }

    pub fn get_result(&self) -> String {
        self.result.clone()
    }

    pub fn get_error(&self) -> String {
        self.error.clone()
    }
}

#[wasm_bindgen]
pub fn parse_ned(input: String) -> ParseResultForJs {
    let tokenvec = all_consuming(tokenizer::lex_tokens)(Span::new(&input));

    match tokenvec {
        Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e))
            => ParseResultForJs::error(format!("tokenizer error:\n{}", convert_error(input.as_str(), e))),
        Ok(r) => {
            let ec = tokens::ErrorsCell::new(vec![]);
            let tokens = Tokens::new(&r.1, &ec);
            let r = all_consuming(nedfile)(tokens);

            match r {
                Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e))
                    => ParseResultForJs::parse_error(tokens,
                        format!("{:#?}", tokens),
                        format!("parser error:n{:#?}", e),
                        // format!("parser error at token:\n{:#?}\n{:#?}", e.errors[0].0.start, tokens.tok[e.errors[0].0.start])
                        // format!("parser error at token:\n{:#?}\n{:#?}", e.errors[0].0.location_offset(), tokens.tok[e.errors[0].0.location_offset()])
                    ),
                Ok(r) => ParseResultForJs::success(tokens, format!("{:#?}", tokens), format!("{:#?}", r)),
                _ => ParseResultForJs::error("unknown parser error".to_string())
            }
        }
        _ => ParseResultForJs::error("unknown tokenizer error".to_string())
    }
}

pub struct ParseError2 {
    pub msg: String,
    pub line: u32
}

use nom::error::VerboseError;

pub fn parse_ned2(input: String) -> Result<types::NedFile, ParseError2> {
    let tokenvec = all_consuming(tokenizer::lex_tokens)(Span::new(input.as_str()));

    match tokenvec {
        Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e)) => {
            Err( ParseError2 { msg : format!("{:#?}", e), line: e.errors.first().unwrap().0.location_line() } )
        },
        Ok(r) => {
            let ec = tokens::ErrorsCell::new(vec![]);
            let tokens = Tokens::new(&r.1, &ec);
            let r = all_consuming(nedfile)(tokens);

            match r {
                Ok(o) => Ok(o.1),
                Err(nom::Err::Failure(e)) => Err(ParseError2 { msg: format!("{:#?}", e.wrapped.errors.first().unwrap().1),
                    line: {
                        let tokens = e.wrapped.errors.first().unwrap().0;
                        tokens.tok[0].content.location_line() - 1
                    }
                }),
                Err(e) => Err(ParseError2 { msg: "whatevzt".to_string(), line:10})
            }
        },
        Err(e) => Err(ParseError2 { msg: "kek".to_string(), line: 4 })

    }
}
