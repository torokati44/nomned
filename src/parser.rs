
use crate::tokens::*;
use crate::tokenizer::*;
use crate::types::*;


use nom::combinator::{map, recognize, value};


use nom::branch::alt;
use nom::multi::{many0, many1, many_till, separated_list};
use nom::sequence::{delimited, preceded, pair, separated_pair, terminated, tuple};

use nom::combinator::{complete, cut, opt};

use nom::multi::{separated_nonempty_list};




use nom::Compare;
use nom::CompareResult;
use nom::IResult;
use nom::InputTake;
use nom::Err;
use nom::error::context;
use nom::error::ErrorKind;
use nom_allerror::errorcollector::CollectingErrorWrapper;

pub type MyParseError<'a> = CollectingErrorWrapper<nom::error::VerboseError<Tokens<'a>>>;

pub type ParseResult<'a, T> = IResult<Tokens<'a>, T, MyParseError<'a>>;



pub trait HasSourceLocation {
    fn set_loc(&mut self, sr : SourceRange);
}

macro_rules! has_source_location {
    ($type:ident) => {
        has_source_location!($type, source_range);
    };

    ($type:ident, $member:ident) => {
        impl HasSourceLocation for $type {
            fn set_loc(&mut self, sr : SourceRange) {
                self.$member = sr;
            }
        }
    };
}


has_source_location!(SimpleModuleDefinition);
has_source_location!(CompoundModuleDefinition);
has_source_location!(ModuleHeader);
has_source_location!(Gate);
has_source_location!(Connection);

pub fn capture_location<'a, O, P>(p: P) -> impl Fn(Tokens<'a>) -> IResult<Tokens<'a>, O, MyParseError<'a>>
where
    O: HasSourceLocation,
    P: Fn(Tokens<'a>) -> IResult<Tokens<'a>, O, MyParseError<'a>>
{
    move |input: Tokens<'a>| {
        let mut sr = SourceRange::default();

        sr.from_line = input.tok[0].content.location_line() as usize;
        sr.from_column = input.tok[0].content.get_utf8_column() as usize;

        let res = p(input);

        match res {
            Ok(mut r) => {
                sr.to_line = r.0.tok[0].preceding_whitespace.location_line() as usize;
                sr.to_column = r.0.tok[0].preceding_whitespace.get_utf8_column() as usize;

                r.1.set_loc(sr);

                Ok(r)

            },
            _ => { res }
        }
    }
}


pub fn tokentag<'a>(tag: TokenKind) -> impl Fn(Tokens<'a>) -> IResult<Tokens<'a>, Tokens<'a>, MyParseError<'a>>
{
    // EH, would be better if we didn't have to do this here
    context(tag.source_appearance(), move |i: Tokens| {
        let res: IResult<_, _, MyParseError<'a>> = match i.compare(tag) {
        CompareResult::Ok => Ok(i.take_split(1)),
        _ => {
            let ek: ErrorKind = ErrorKind::Tag;
            let er : MyParseError = nom::error::make_error(i, ek);
            Err(Err::Error(er ))
        }
        };
        res
    })
}


pub fn importname<'a>(input : Tokens<'a>) -> ParseResult<'a,Tokens<'a>> {
    recognize(many1(
        alt((
            tokentag(TokenKind::Name),
            tokentag(TokenKind::DoubleAsterisk),
            tokentag(TokenKind::Asterisk)
        ))
    ))(input)
}

pub fn importspec<'a>(input : Tokens<'a>) -> ParseResult<'a,Tokens<'a>> {
    recognize(separated_nonempty_list(tokentag(TokenKind::Period), importname))(input)
}

pub fn import<'a>(input : Tokens<'a>) -> ParseResult<'a,Tokens<'a>> {
    delimited(
        tokentag(TokenKind::Import),
        importspec,
        tokentag(TokenKind::SemiColon),
    )(input)
}

pub fn dottedname<'a>(input: Tokens<'a>) -> ParseResult<'a,Tokens<'a>> {
    recognize(separated_nonempty_list(tokentag(TokenKind::Period), tokentag(TokenKind::Name)))(input)
}

pub fn packagedeclaration<'a>(input : Tokens<'a>) -> ParseResult<'a,Tokens<'a>> {
    delimited(
        tokentag(TokenKind::Package),
        dottedname,
        tokentag(TokenKind::SemiColon),
    )(input)
}


pub fn prop_lit_sign(input: Tokens) -> ParseResult<Tokens> {
    alt(( // one_of("-.?:/#,") // underscore? TODO check
        tokentag(TokenKind::PlusSign),
        tokentag(TokenKind::MinusSign),
        tokentag(TokenKind::Period),
        tokentag(TokenKind::Questionmark),
        tokentag(TokenKind::Colon),
        tokentag(TokenKind::DoubleColon),
        tokentag(TokenKind::Slash),
        tokentag(TokenKind::Hashmark),
        tokentag(TokenKind::Comma),
    ))(input)
}

/*
pub fn property_literal_inside(input: Tokens) -> ParseResult<Tokens> {
    recognize(many1(alt((
        recognize(delimited(tokentag(TokenKind::ParenLeft), property_literal_inside, tokentag(TokenKind::ParenRight))),
        recognize(tokentag(TokenKind::StringConstant)),
        recognize(tokentag(TokenKind::IntConstant)),
        tokentag(TokenKind::Name),
        recognize(prop_lit_sign),
    ))))(input)
}
*/
pub fn property_literal(input: Tokens) -> ParseResult<Tokens> {
    recognize(many1(alt((
        recognize(delimited(tokentag(TokenKind::ParenLeft), property_literal /*_inside */, tokentag(TokenKind::ParenRight))),
        tokentag(TokenKind::StringConstant),
        tokentag(TokenKind::IntConstant),
        realconstant_ext,
        tokentag(TokenKind::Name),
        prop_lit_sign
    ))))(input)
}

pub fn propname(input: Tokens) -> ParseResult<Tokens> {

    pub fn symbols(input: Tokens) -> ParseResult<Tokens> {
        alt((
            tokentag(TokenKind::Colon),
            tokentag(TokenKind::Period),
            tokentag(TokenKind::PlusSign),
            tokentag(TokenKind::MinusSign),
        ))(input)
    }

    complete(recognize(
            many1(alt((
                tokentag(TokenKind::Name),
                recognize(symbols)
        )))
    ))(input)
}


fn property_values(input: Tokens) -> ParseResult<Vec<Tokens>> {
    // TODO: this does not allow the list to start with ,
    map(
        many0(alt((tokentag(TokenKind::Comma), property_literal))),
        |v| v.iter().map(|s| s.to_owned()).collect(),
    )(input)
}

fn property_name(input: Tokens) -> ParseResult<PropertyNameIndex> {
    let index_parser = delimited(
        tokentag(TokenKind::SquareLeft),
        propname,
        tokentag(TokenKind::SquareRight)
    );
    let boop_parser = pair(preceded(tokentag(TokenKind::At), propname), opt(index_parser));

        map(boop_parser, |t| PropertyNameIndex {
            name: t.0.get_content(),
            index: t.1.map(|s| s.get_content()),
        })
    (input)
}

fn property_key(input: Tokens) -> ParseResult<PropertyKey> {
    // TODO: make =value optional
    alt((
        map(
            separated_pair(
                property_literal,
                tokentag(TokenKind::EqualsSign),
                property_values,
            ),
            |t| PropertyKey {
                key: Some(t.0.get_content()),
                values: t.1.iter().map(|s| s.get_content()).collect(),
            },
        ),
        map(property_values, |t| PropertyKey {
            key: None,
            values: t.iter().map(|s| s.get_content()).collect(),
        }),
    ))(input)
}

fn property_keys(input: Tokens) -> ParseResult<Vec<PropertyKey>> {
    separated_list( tokentag(TokenKind::SemiColon), property_key)(input)
}

fn property_namevalue(input: Tokens) -> ParseResult<Property> {
    // TODO dunno why it won't accept the empty list: ()
    let parser = pair(
        property_name,
        opt(delimited(tokentag(TokenKind::ParenLeft), opt(property_keys), tokentag(TokenKind::ParenRight))),
    );
    map(parser, |(n, k)| Property {
        nameindex: n.to_owned(),
        keys: k.unwrap_or(Option::None).unwrap_or(vec![]),
    })(input)
}


pub fn dottednames(input: Tokens) -> ParseResult<Vec<Tokens>> {
    separated_nonempty_list( tokentag(TokenKind::Comma), dottedname)(input)
}


fn binary_op<'a>(input: Tokens) -> ParseResult<Tokens> {
    alt((
        tokentag(TokenKind::Eq),
        tokentag(TokenKind::Ne),
        tokentag(TokenKind::Ge),
        tokentag(TokenKind::Le),
        tokentag(TokenKind::And),
        tokentag(TokenKind::Or),
        tokentag(TokenKind::Xor),
        tokentag(TokenKind::ShiftLeft),
        tokentag(TokenKind::ShiftRight),
        // recognize(one_of("+-*/%^<&|#>")), // - TODO
    ))(input)
}

pub fn quantity(input: Tokens) -> ParseResult<Tokens> {
    recognize(many1(
        pair(alt((recognize(tokentag(TokenKind::IntConstant)), realconstant_ext)), tokentag(TokenKind::Name)),
    ))(input)
}

pub fn realconstant_ext(input: Tokens) -> ParseResult<Tokens> {
    alt((tokentag(TokenKind::RealConstant), tokentag(TokenKind::Inf), tokentag(TokenKind::Nan)))(input)
}

pub fn numliteral(input: Tokens) -> ParseResult<Tokens> {
    alt((recognize(tokentag(TokenKind::IntConstant)), realconstant_ext, quantity))(input)
}

pub fn literal<'a>(input: Tokens) -> ParseResult<Tokens> {
    alt((
        recognize(tokentag(TokenKind::StringConstant)),                // stringliteral
        alt((tokentag(TokenKind::True), tokentag(TokenKind::False))), // boolliteral
        numliteral,
    ))(input)
}


pub fn identifier(input: Tokens) -> ParseResult<Tokens> {
    alt((
        recognize(separated_pair(tokentag(TokenKind::This), tokentag(TokenKind::Period), tokentag(TokenKind::Name))),
        recognize(separated_pair(tokentag(TokenKind::Name), tokentag(TokenKind::Period), tokentag(TokenKind::Name))),
        recognize(separated_pair(
            terminated(tokentag(TokenKind::Name), delimited(tokentag(TokenKind::SquareLeft), expr, tokentag(TokenKind::SquareRight))),
            tokentag(TokenKind::Period),
            tokentag(TokenKind::Name),
        )),
        tokentag(TokenKind::Name),
    ))(input)
}

pub fn operator(input: Tokens) -> ParseResult<Tokens> {
    alt((
        recognize(pair(tokentag(TokenKind::Index), pair(tokentag(TokenKind::ParenLeft), tokentag(TokenKind::ParenRight)))),
        recognize(pair(
            tokentag(TokenKind::Exists),
            delimited(tokentag(TokenKind::ParenLeft), identifier, tokentag(TokenKind::ParenRight)),
        )),
        recognize(pair(
            tokentag(TokenKind::Sizeof),
            delimited(tokentag(TokenKind::ParenLeft), identifier, tokentag(TokenKind::ParenRight)),
        )),
        tokentag(TokenKind::Index),
        tokentag(TokenKind::Typename),
    ))(input)
}


pub fn simple_expr(input: Tokens) -> ParseResult<Tokens> {
    alt((identifier, operator, literal))(input)
}

pub fn expr_token(input: Tokens) -> ParseResult<Tokens> {
    alt((
        //expr_func,
        binary_op,
        simple_expr,
        // recognize(one_of("-!~?:")), // - TODO
    ))(input)
}

fn expr_inside(input: Tokens) -> ParseResult<Tokens> {
    recognize(
        many1(
            alt((
                tokentag(TokenKind::Comma),
                expr_token,
                recognize(delimited(
                    tokentag(TokenKind::ParenLeft),
                        expr_inside,
                    tokentag(TokenKind::ParenRight),
                ))
            ))
    ))(input)
}

fn expr(input: Tokens) -> ParseResult<Tokens> {
    recognize(
        many1(
            alt((
                expr_token,
                    recognize(delimited(
                        tokentag(TokenKind::ParenLeft),
                        expr_inside,
                        tokentag(TokenKind::ParenRight),
                    ),
                ),
            ))
        ),
    )(input)
}


pub fn expression(input: Tokens) -> ParseResult<Expression> {
    map(crate::expr::expression, |e| Expression { expression: e.get_content() })(input)
}


pub fn inheritance(input: Tokens) -> ParseResult<Inheritance> {
    map(
        pair(
            opt(preceded(
                tokentag(TokenKind::Extends),
                dottedname,
            )),
            opt(preceded(tokentag(TokenKind::Like), dottednames)),
        ),
        |(e, l)| Inheritance {
            extends: e.map(|s| s.get_content()),
            like: l.unwrap_or(vec![]).iter()
            .map(|e| e.get_content())
            .collect::<Vec<String>>()
        },
    )(input)
}

pub fn moduleheader(input: Tokens) -> ParseResult<ModuleHeader> {
    let parser = map(
        pair(
            pair(
                    alt((
                        tokentag(TokenKind::Simple),
                        tokentag(TokenKind::Module),
                        tokentag(TokenKind::Network)
                    )),
                tokentag(TokenKind::Name),
            ),
            opt(inheritance),
        ),
        |((k, n), i)| ModuleHeader {
            kind: k.get_content(),
            name: n.get_content(),
            inheritance: i.unwrap_or(Inheritance {
                extends: None,
                like: vec![],
            }),
            source_range: SourceRange::default()
        },
    );

    capture_location(parser)(input)
}

fn gatetype(input: Tokens) -> ParseResult<GateType> {
    let parse_input = value(GateType::INPUT, tokentag(TokenKind::Input));
    let parse_output = value(GateType::OUTPUT, tokentag(TokenKind::Output));
    let parse_inout = value(GateType::INOUT, tokentag(TokenKind::InOut));

    alt((parse_input, parse_output, parse_inout))(input)
}

fn vector(input: Tokens) -> ParseResult<Vector> {
    map(
        delimited(tokentag(TokenKind::SquareLeft), expression, tokentag(TokenKind::SquareRight)),
        |t| Vector { expression: t },
    )(input)
}


fn gate_typenamesize(input: Tokens) -> ParseResult<GateTypeNameSize> {
    alt((
        map(
            tuple((gatetype, tokentag(TokenKind::Name), vector)),
            |t| GateTypeNameSize {
                gatetype: t.0,
                name: t.1.get_content(),
                size: 0,
            },
        ),
        map(
            tuple((
                gatetype,
                tokentag(TokenKind::Name),
                tokentag(TokenKind::SquareLeft),
                tokentag(TokenKind::SquareRight),
            )),
            |t| GateTypeNameSize {
                gatetype: t.0,
                name: t.1.get_content(),
                size: 0,
            },
        ),
        map(tuple((gatetype, tokentag(TokenKind::Name))), |t| GateTypeNameSize {
            gatetype: t.0,
            name: t.1.get_content(),
            size: 0,
        }),
        map(tuple((tokentag(TokenKind::Name), vector)), |t| GateTypeNameSize {
            gatetype: GateType::UNSPEC,
            name: t.0.get_content(),
            size: 0,
        }),
        map(
            tuple((tokentag(TokenKind::Name),tokentag(TokenKind::SquareLeft), tokentag(TokenKind::SquareRight))),
            |t| GateTypeNameSize {
                gatetype: GateType::UNSPEC,
                name: t.0.get_content(),
                size: 0,
            },
        ),
        map(tokentag(TokenKind::Name), |n| GateTypeNameSize {
            gatetype: GateType::UNSPEC,
            name: n.get_content(),
            size: 0,
        }),
    ))(input)
}


fn opt_inline_properties(input: Tokens) -> ParseResult<Vec<Property>> {
    many0(property_namevalue)(input) // separated_list didn't work for some reason??
}


fn gate(input: Tokens) -> ParseResult<Gate> {
    let parser = terminated(
        pair(
            gate_typenamesize,
             opt_inline_properties
        ),
        tokentag(TokenKind::SemiColon),
    );
    let mapper = map(parser, |t| Gate {
        typenamesize: t.0,
        properties: t.1,
        source_range: SourceRange::default()
    });
    capture_location(mapper)(input)
}

fn gateblock(input: Tokens) -> ParseResult<Vec<Gate>> {
    let gatestag = pair(tokentag(TokenKind::Gates), tokentag(TokenKind::Colon));
    preceded(
        gatestag,
        cut(many0(gate)),
    )(input)
}

fn pattern(input: Tokens) -> ParseResult<Pattern> {
    map(
        recognize(pair(pattern2, opt(preceded(tokentag(TokenKind::Period), tokentag(TokenKind::Typename))))),
        |s| Pattern {
            pattern: s.get_content(),
        },
    )(input)
}


fn pattern2(input: Tokens) -> ParseResult<Tokens> {
    recognize(separated_nonempty_list(tokentag(TokenKind::Period), pattern_elem))(input)
}



fn pattern_starter(input: Tokens) -> ParseResult<Tokens> {
    alt((
        recognize(tokentag(TokenKind::Channel)),
        recognize(separated_pair(tokentag(TokenKind::Name), tokentag(TokenKind::DollarSign), tokentag(TokenKind::Name))),
    ))(input)
}

fn pattern_cont(input: Tokens) -> ParseResult<Tokens> {
    alt((
        recognize(tokentag(TokenKind::Name)),
        recognize(delimited(tokentag(TokenKind::CurlyLeft), pattern_index, tokentag(TokenKind::CurlyRight))),
        recognize(tokentag(TokenKind::Asterisk)),
    ))(input)
}

// TODO: make this return not a String
pub fn pattern_name(input: Tokens) -> ParseResult<String> {
    map(
        pair(alt((pattern_starter, pattern_cont)), many0(pattern_cont)),
        |(a, b)| {
            [
                a.get_content(),
                b.iter()
                    .map(|e| e.get_content())
                    .collect::<Vec<String>>()
                    .concat(),
            ]
            .concat()
        },
    )(input)
}


fn pattern_elem(input: Tokens) -> ParseResult<String> {
    map(
        recognize(alt((
            tokentag(TokenKind::DoubleAsterisk),
            recognize(pair(
                pattern_name,
                delimited(
                    tokentag(TokenKind::SquareLeft),
                    alt((recognize(pattern_index), tokentag(TokenKind::Asterisk))),
                    tokentag(TokenKind::SquareRight),
                ),
            )),
            recognize(pattern_name),
        ))),
        |s| s.get_content(),
    )(input)
}

fn pattern_index(input: Tokens) -> ParseResult<PatternIndex> {
    alt((
        // the order matters maybe too much???
        map(
            preceded(tokentag(TokenKind::To),  tokentag(TokenKind::IntConstant)),
            |i| PatternIndex {
                from: None,
                to: Some(i),
            },
        ),
        map(
            separated_pair(
                tokentag(TokenKind::IntConstant),
                tokentag(TokenKind::To),
                tokentag(TokenKind::IntConstant),
            ),
            |(f, t)| PatternIndex {
                from: Some(f),
                to: Some(t),
            },
        ),
        map(
            terminated(tokentag(TokenKind::IntConstant), tokentag(TokenKind::To)),
            |i| PatternIndex {
                from: Some(i),
                to: None,
            },
        ),
        map(tokentag(TokenKind::IntConstant), |c| PatternIndex {
            from: Some(c),
            to: Some(c),
        }),
    ))(input)
}


fn paramvalue(input: Tokens) -> ParseResult<ParamValue> {
    alt((
        map(
            preceded(
                tokentag(TokenKind::Default),
                delimited(tokentag(TokenKind::ParenLeft), expression, tokentag(TokenKind::ParenRight)),
            ),
            |e| ParamValue::DefExpr(e),
        ),
        value(ParamValue::DEFAULT, tokentag(TokenKind::Default)),
        value(ParamValue::ASK, tokentag(TokenKind::Ask)),
        map(expression, |e| ParamValue::Expr(e)),
    ))(input)
}

fn pattern_value(input: Tokens) -> ParseResult<PatternValue> {
    terminated(
        map(
            separated_pair(
                pattern,
                 tokentag(TokenKind::EqualsSign),
                paramvalue,
            ),
            |(p, pv)| PatternValue {
                pattern: p,
                value: pv,
            },
        ),
        tokentag(TokenKind::SemiColon),
    )(input)
}



fn paramtype(input: Tokens) -> ParseResult<ParamType> {
    alt((
        value(ParamType::DOUBLE, tokentag(TokenKind::Double)),
        value(ParamType::INT, tokentag(TokenKind::Int)),
        value(ParamType::STRING, tokentag(TokenKind::String)),
        value(ParamType::BOOL, tokentag(TokenKind::Bool)),
        value(ParamType::XML, tokentag(TokenKind::Xml)),
    ))(input)
}


pub fn param_typename(input: Tokens) -> ParseResult<ParamTypeName> {
    alt((
        map(
            pair(
                pair(opt(tokentag(TokenKind::Volatile)), paramtype),
                tokentag(TokenKind::Name),
            ),
            |((v, t), n)| ParamTypeName {
                volatile: v.is_some(),
                paramtype: Some(t),
                name: n.get_content(),
            },
        ),
        map(tokentag(TokenKind::Name), |n| ParamTypeName {
            volatile: false,
            paramtype: None,
            name: n.get_content(),
        }),
    ))(input)
}


fn param_typenamevalue(input: Tokens) -> ParseResult<ParamTypeNameValue> {
    let left_parser = pair(param_typename, opt_inline_properties);
    let right_parser = alt((
        value((None, vec![]), tokentag(TokenKind::SemiColon)),
        preceded(
            tokentag(TokenKind::EqualsSign),
            map(
                terminated(
                    pair(paramvalue, opt_inline_properties),
                    tokentag(TokenKind::SemiColon),
                ),
                |(v, p)| (Some(v), p),
            ),
        ),
    ));
    map(
        pair(left_parser, right_parser),
        |(left, right)| ParamTypeNameValue {
            typename: left.0,
            properties_left: left.1,
            paramvalue: right.0,
            properties_right: right.1,
        },
    )(input)
}


fn param(input: Tokens) -> ParseResult<ParamOrPattern> {
    alt((
        map(param_typenamevalue, |tnv| ParamOrPattern::Param(tnv)),
        map(pattern_value, |pv| ParamOrPattern::Pattern(pv)),
    ))(input)
}

fn property(input: Tokens) -> ParseResult<Property> {
    terminated(property_namevalue, tokentag(TokenKind::SemiColon))(input)
}

fn paramsitem(input: Tokens) -> ParseResult<ParamsItem> {
    alt((
        map(param, |p| ParamsItem::ParamOrPattern(p)),
        map(property, |p| ParamsItem::Property(p)),
    ))(input)
}

pub fn opt_paramblock(input: Tokens) -> ParseResult<Vec<ParamsItem>> {
    preceded(
        opt(pair(tokentag(TokenKind::Parameters), tokentag(TokenKind::Colon)) ),
        cut(many0(paramsitem)),
    )(input)
}

pub fn simplemoduledefinition(input: Tokens) -> ParseResult<SimpleModuleDefinition> {
    let parser = pair(
        moduleheader,
        delimited(
            tokentag(TokenKind::CurlyLeft),
            pair(opt_paramblock, opt(gateblock)),
            tokentag(TokenKind::CurlyRight)
        ),
    );

    capture_location(map(parser, |(h, (p, g))| SimpleModuleDefinition {
        header: h,
        params: p,
        gates: g.unwrap_or(vec![]),
        source_range: SourceRange::default()
    }))(input)
}


pub fn channelheader(input: Tokens) -> ParseResult<ChannelHeader> {
    map(
        pair(
            preceded(tokentag(TokenKind::Channel),  tokentag(TokenKind::Name)),
            opt(inheritance),
        ),
        |(n, i)| ChannelHeader {
            name: n.get_content(),
            inheritance: i.unwrap_or(Inheritance {
                extends: None,
                like: vec![],
            }),
        },
    )(input)
}

pub fn channeldefinition(input: Tokens) -> ParseResult<ChannelDefinition> {
    let parser = pair(
        channelheader,
        cut(delimited(
            tokentag(TokenKind::CurlyLeft),
            opt_paramblock,
            tokentag(TokenKind::CurlyRight),
        )),
    );

    map(parser, |(h, p)| ChannelDefinition {
        header: h,
        params: p,
    })(input)
}

fn localtype(input: Tokens) -> ParseResult<LocalType> {
    terminated(
        alt((
            map(channeldefinition, |c| LocalType::Channel(c)),
            map(simplemoduledefinition, |s| LocalType::SimpleModule(s)),
            map(compoundmoduledefinition, |c| LocalType::CompoundModule(c)),
            // TODO: many more cases
        )),
        opt(tokentag(TokenKind::SemiColon)),
    )(input)
}

fn typeblock(input: Tokens) -> ParseResult<Vec<LocalType>> {
    let typestag = pair(tokentag(TokenKind::Types), tokentag(TokenKind::Colon));
    preceded(
        typestag,
        many0(localtype),
    )(input)
}


fn likeexpr(input: Tokens) -> ParseResult<LikeExpr> {

    alt((
        value(LikeExpr::Empty, tokentag(TokenKind::Spaceship)),
        delimited(
            tokentag(TokenKind::PointyLeft),
            alt((
                map(

                    preceded(
                        tokentag(TokenKind::Default),
                        delimited(
                            tokentag(TokenKind::ParenLeft),
                            recognize(expression),
                            tokentag(TokenKind::ParenRight)
                        )
                    ),
                    |e| {
                        LikeExpr::DefExpr(Expression { expression: e.get_content() })
                    }
                ),
                map(
                    opt(recognize(expression)),
                    |e| {
                        match e {
                            Some(e) => LikeExpr::Expr(Expression { expression: e.get_content() }),
                            None =>  LikeExpr::Empty
                        }
                    }
                )
            )),
            tokentag(TokenKind::PointyRight),
        ))
    )(input)
}

fn submoduleheader(input: Tokens) -> ParseResult<SubmoduleHeader> {
    map(
        pair(
            separated_pair(
                pair(tokentag(TokenKind::Name), opt(vector)),
                tokentag(TokenKind::Colon),
                pair(
                    opt(terminated(likeexpr, tokentag(TokenKind::Like))),
                    dottedname,
                ),
            ),
            opt(map(
                // TODO: factor this out as condition()
                preceded( tokentag(TokenKind::If),  expression),
                |e| Condition { expression: e },
            )),
        ),
        |(((n, v), (l, t)), c)| SubmoduleHeader {
            name: n.get_content(),
            likeexpr: l,
            moduletype: t.get_content(),
            vector: v,
            condition: c,
        },
    )(input)
}

fn submodule(input: Tokens) -> ParseResult<Submodule> {
    alt((
        map(terminated(submoduleheader, tokentag(TokenKind::SemiColon)), |h| Submodule {
            header: h,
            params: vec![],
            gates: vec![],
        }),
        map(
            pair(
                submoduleheader,
                terminated(
                    delimited(
                         tokentag(TokenKind::CurlyLeft),
                        pair(opt_paramblock, opt(gateblock)),
                         tokentag(TokenKind::CurlyRight),
                    ),
                    opt(tokentag(TokenKind::SemiColon)),
                ),
            ),
            |(h, (p, g))| Submodule {
                header: h,
                params: p,
                gates: g.unwrap_or(vec![]),
            },
        ),
    ))
    (input)
}

fn submodblock(input: Tokens) -> ParseResult<Vec<Submodule>> {
    let submodstag = pair(tokentag(TokenKind::Submodules), tokentag(TokenKind::Colon));
    preceded(
        submodstag,
        cut(many0(submodule)),
    )(input)
}


fn loops_and_conditions(input: Tokens) -> ParseResult<Vec<LoopOrCondition>> {
    separated_nonempty_list(
        tokentag(TokenKind::Comma),
        alt((
            map(
                preceded(tokentag(TokenKind::If), expression),
                |e| LoopOrCondition::Condition(Condition { expression: e }),
            ),
            map(
                pair(
                    delimited(
                        tokentag(TokenKind::For),
                        tokentag(TokenKind::Name),
                        tokentag(TokenKind::EqualsSign),
                    ),
                    separated_pair(expression, tokentag(TokenKind::To), expression),
                ),
                |(n, (e1, e2))| {
                    LoopOrCondition::Loop(Loop {
                        variable: n.get_content(),
                        from_expression: e1,
                        to_expression: e2,
                    })
                },
            ),
        )),
    )(input)
}


fn gatespec(input: Tokens) -> ParseResult<Tokens> {
    alt((
        recognize(separated_pair(
            pair(tokentag(TokenKind::Name), opt(vector)),
            tokentag(TokenKind::Period),
            pair(
                pair(tokentag(TokenKind::Name), opt(pair(tokentag(TokenKind::DollarSign), tokentag(TokenKind::Name)))),
                opt(alt((tokentag(TokenKind::PlusPlus), recognize(vector)))),
            ),
        )),
        // parent:
        recognize(pair(
            pair(tokentag(TokenKind::Name), opt(pair(tokentag(TokenKind::DollarSign), tokentag(TokenKind::Name)))),
            opt(alt((tokentag(TokenKind::PlusPlus), recognize(vector)))),
        )),
    ))(input)
}

fn channelspec_header(input: Tokens) -> ParseResult<ChannelSpecHeader> {
    let channelname = opt(terminated(tokentag(TokenKind::Name), tokentag(TokenKind::Colon)));
    // TODO: likeexpr, dottedname (type)
    map(terminated(channelname, opt(dottedname)), |n| {
        ChannelSpecHeader {
            channelname: n.map(|s| s.get_content()),
            channeltype: None,
            like: None,
        }
    })(input)
}

fn channelspec(input: Tokens) -> ParseResult<ChannelSpec> {
    map(
        pair(
            channelspec_header,
            opt(delimited(tokentag(TokenKind::CurlyLeft), opt_paramblock, tokentag(TokenKind::CurlyRight))),
        ),
        |(h, p)| ChannelSpec {
            header: h,
            params: p.unwrap_or(vec![]),
        },
    )(input)
}

fn connection(input: Tokens) -> ParseResult<Connection> {
    fn arrow(input: Tokens) -> ParseResult<Tokens> {
        alt((
            tokentag(TokenKind::DoubleArrow),
            tokentag(TokenKind::RightArrow),
            tokentag(TokenKind::LeftArrow
            ))
        )(input)
    }

    capture_location(map(
        tuple((
            terminated(gatespec, arrow),
            opt(terminated(channelspec, arrow)),
            gatespec,
        )),
        |(l, c, r)| Connection {
            left: l.get_content(),
            channelspec: c,
            right: r.get_content(),
            source_range: SourceRange::default()
        },
    ))(input)
}


fn connectionsitem(input: Tokens) -> ParseResult<ConnectionsItem> {
    // TODO: group, loops, conditions...
    alt((
        map(
            terminated(
                pair(
                    opt(loops_and_conditions),
                    delimited(
                         tokentag(TokenKind::CurlyLeft),
                        many0(connectionsitem),
                        tokentag(TokenKind::CurlyRight),
                    ),
                ),
                opt(tokentag(TokenKind::SemiColon)),
            ),
            |(lc, ci)| {
                ConnectionsItem::ConnectionGroup(ConnectionGroup {
                    loops_and_conditions: lc.unwrap_or(vec![]),
                    connectionsitems: ci,
                })
            },
        ),
        map(
            terminated(pair(connection, opt(loops_and_conditions)), tokentag(TokenKind::SemiColon)),
            |(c, lc)| ConnectionsItem::Connection(c, lc.unwrap_or(vec![])),
        ),
    ))(input)
}

fn connblock(input: Tokens) -> ParseResult<Vec<ConnectionsItem>> {
    let connstag = pair(
        pair(
            tokentag(TokenKind::Connections),
            opt(tokentag(TokenKind::Allowunconnected)),
        ),
        tokentag(TokenKind::Colon),
    );
    preceded(
        connstag,
        cut(many0(connectionsitem)),
    )(input)
}


pub fn compoundmoduledefinition(input: Tokens) -> ParseResult<CompoundModuleDefinition> {
    let parser = pair(
        moduleheader,
        cut(delimited(
            tokentag(TokenKind::CurlyLeft),
            tuple((
                opt_paramblock,
                opt(gateblock),
                opt(typeblock),
                opt(submodblock),
                opt(connblock),
            )),
            tokentag(TokenKind::CurlyRight)
        )),
    );

        capture_location(map(parser, |(h, (p, g, t, s, c))| CompoundModuleDefinition {
            header: h,
            params: p,
            gates: g.unwrap_or(vec![]),
            types: t.unwrap_or(vec![]),
            submods: s.unwrap_or(vec![]),
            conns: c.unwrap_or(vec![]),
            source_range: SourceRange::default()
        }))(input)
}


pub fn interfaceinheritance(input: Tokens) -> ParseResult<InterfaceInheritance> {
    map(
        opt(preceded(
            tokentag(TokenKind::Extends),
            dottednames,
        )),
        |e| InterfaceInheritance {
            extends: e.unwrap_or(vec![]).iter().map(|s| s.get_content()).collect(),
        },
    )(input)
}

pub fn moduleinterfaceheader(input: Tokens) -> ParseResult<ModuleInterfaceHeader> {
    map(
        pair(
            pair(tokentag(TokenKind::ModuleInterface), tokentag(TokenKind::Name)),
            opt(interfaceinheritance),
        ),
        |((k, n), i)| ModuleInterfaceHeader {
            name: n.get_content(),
            inheritance: i.unwrap_or(InterfaceInheritance { extends: vec![] }),
        },
    )(input)
}


pub fn moduleinterfacedefinition(input: Tokens) -> ParseResult<ModuleInterfaceDefinition> {
    let parser = pair(
        moduleinterfaceheader,
        cut(delimited(
            tokentag(TokenKind::CurlyLeft),
            pair(
                opt_paramblock,
                opt(gateblock),
            ),
            tokentag(TokenKind::CurlyRight)
        )),
    );

    map(parser, |(h, (p, g))| ModuleInterfaceDefinition {
        header: h,
        params: p,
        gates: g.unwrap_or(vec![]),
    })(input)
}



pub fn definition<'a>(input : Tokens<'a>) -> ParseResult<'a,Definition> {
    terminated(
        alt((
            map(packagedeclaration, |pd| Definition::PackageDeclaration(pd.get_content())),
            map(import, |i| Definition::Import(i.get_content())),
            map(terminated(property_namevalue, tokentag(TokenKind::SemiColon)), |fp| {
                Definition::FileProperty(fp)
            }),
            map(simplemoduledefinition, |smd| {
                Definition::SimpleModuleDefinition(smd)
            }),
            map(compoundmoduledefinition, |cmd| {
                Definition::CompoundModuleDefinition(cmd)
            }),
            map(moduleinterfacedefinition, |mid| {
                Definition::ModuleInterfaceDefinition(mid)
            }),
            map(channeldefinition, |cd| Definition::ChannelDefinition(cd)),
        )),
        opt(tokentag(TokenKind::SemiColon)),
    )(input)
}

pub fn nedfile<'a>(input : Tokens<'a>) -> ParseResult<'a,NedFile> {
    map(
        many_till(definition, tokentag(TokenKind::EOF)),
        |(d, eof)| NedFile { definitions: d },
    )(input)
}


fn combined<'a, 'b : 'a>(input: Span<'a>) {
    let tokenvec = lex_tokens(input).unwrap().1;

    println!("{:#?}", tokenvec);
    let ec = ErrorsCell::new(vec![]);
    let tokens = Tokens::new(&tokenvec, &ec);
    let r = nedfile(tokens);
}


pub fn main() {
    println!("{:#?}", token(Span::new(" /*  gjklkg */  // pog \n import")));
}


#[test]
fn test_combined() {
    // " /*  gjklkg */  // pog \n import \n simple module"
    println!("{:#?}", combined(Span::new(" /*  gjklkg */ simple  { \n : : \n : : } \n")));
}
