extern crate nomned;
extern crate text_io;

pub mod common;
pub mod expr;
pub mod types;
pub mod tokenizer;
pub mod parser;

use crate::common::*;
use crate::expr::*;
use crate::parser::*;
use nomned::*;

use nom::combinator::{all_consuming, recognize};
use text_io::read;

fn main() {
    //println!("{:#?}", all_consuming(pattern_name)(Span::new("channel")));
    //println!("{:#?}", all_consuming(property_literal)(Span::new("\"i6\\\"66hh\"")));
/*
    println!(
        "{:#?}",
        recognize(all_consuming(param_typename))(Span::new("double param1"))
    );

    println!(
        "{:#?}",
        all_consuming(opt_paramblock)(Span::new("parameters: int address;"))
    );
*/
    loop {
        let line: String = read!("{}\n");
        if line.is_empty() {
            break;
        }
        println!("{:#?}", wasm::parse_ned(line));
        //println!("{:#?}", all_consuming(nedfile)(Span::new(&line)));
    }

    /*
        println!("{:#?}", all_consuming(pattern_name)("channel"));
        println!("{:#?}", all_consuming(pattern_name)("*"));
        println!("{:#?}", all_consuming(pattern_name)("sadas*sss*ddd{..40}das"));

        println!("{:#?}", all_consuming(pattern_name)("{30..40}"));
        println!("{:#?}", all_consuming(pattern_name)("phys$in"));
        println!("{:#?}", all_consuming(pattern_name)("ph{30..50}dd"));
    */

    //println!("{:#?}", all_consuming(param_typenamevalue)("double param1 @pog(aa=3) = ask @lel @okay @heheh(abab=33) ;"));
    //assert!(all_consuming(gateblock)("gates: input a @pog @champ[f3](aa=1,2;bc=e) @kek; output b;").is_ok());
}
