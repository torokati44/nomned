extern crate nom;
extern crate nom_greedyerror;
extern crate nom_locate;

pub mod common;
pub mod expr;
pub mod types;
pub mod wasm;
pub mod tokens;
pub mod tokenizer;
pub mod parser;

use common::*;
use expr::*;

use nom::combinator::{all_consuming};

use nom_greedyerror::convert_error;


///////////////////////////////////////////////////////////
/*

#[test]
fn parse_pattern() {
    assert!(all_consuming(pattern)(Span::new("ph")).is_ok());
    assert!(all_consuming(pattern)(Span::new("ee.typename")).is_ok());
    assert!(all_consuming(pattern)(Span::new("sdds.ee.typename")).is_ok());
    assert!(all_consuming(pattern)(Span::new("ph{30..50}dd.sdds.**.aa")).is_ok());
    assert!(all_consuming(pattern)(Span::new("ph{30..50}dd.sdds.ee.typename")).is_ok());

    assert!(all_consuming(pattern)(Span::new("channel")).is_ok());
    assert!(all_consuming(pattern)(Span::new("*")).is_ok());
    assert!(all_consuming(pattern)(Span::new("sadasdas")).is_ok());

    assert!(all_consuming(pattern)(Span::new("{30..40}")).is_ok());
    assert!(all_consuming(pattern)(Span::new("phys$in")).is_ok());
}

#[test]
fn parse_param_typename() {
    assert!(all_consuming(param_typename)(Span::new("double param1")).is_ok());
    assert!(all_consuming(param_typename)(Span::new("volatile int param2")).is_ok());
    assert!(all_consuming(param_typename)(Span::new("param3")).is_ok());

    assert!(all_consuming(param_typenamevalue)(Span::new("double param1;")).is_ok());
    assert!(all_consuming(param_typenamevalue)(Span::new("volatile int param2;")).is_ok());
    assert!(all_consuming(param_typenamevalue)(Span::new("param3;")).is_ok());
}

#[test]
fn parse_param_typenamevalue() {
    assert!(all_consuming(param_typenamevalue)(Span::new("double param1;")).is_ok());
    assert!(all_consuming(param_typenamevalue)(Span::new("double param1 = default;")).is_ok());
    assert!(all_consuming(param_typenamevalue)(Span::new(
        "double param1 @pog(aa=3) = ask @lel @okay @hmm(ababa=ee);"
    ))
    .is_ok());
}

#[test]
fn parse_gateblock() {
    assert!(all_consuming(gateblock)(Span::new("gates: input a; output b;")).is_ok());
    assert!(all_consuming(gateblock)(Span::new(
        "gates: input a @pog @champ[f3](aa=1,2;bc=e) @kek; output b;"
    ))
    .is_ok());
}

#[test]
fn parse_property() {
    assert!(all_consuming(property)(Span::new("@boop;")).is_ok());
    assert!(all_consuming(property)(Span::new("@baap();")).is_ok());
    assert!(all_consuming(property)(Span::new("@baap(rrr);")).is_ok());
    assert!(all_consuming(property)(Span::new("@baap(rrr=43);")).is_ok());

    assert!(all_consuming(property)(Span::new("@baap(rrr=43,33,dd,45);")).is_ok());
    assert!(all_consuming(property)(Span::new("@baap(rrr=43,,dd,45);")).is_ok());
    assert!(all_consuming(property)(Span::new(
        "@baap(rrr=43,,dd,45;aa=34,55,,3;uu=ss;eeee;wowo=oeo);"
    ))
    .is_ok());
    assert!(all_consuming(property)(Span::new(
        "@baap(rrr=43,,dd,45;aa=34,55,,3;uu=ss;eeee;wowo=oeo);"
    ))
    .is_ok());

    assert!(all_consuming(property)(Span::new("@display(a=\"blockbrowser\");")).is_ok());
    assert!(all_consuming(property)(Span::new("@display(\"blockbrowser\");")).is_ok());
}

#[test]
fn parse_gate() {
    assert_eq!(
        gate(Span::new("inout pog1;")).unwrap().1,
        Gate {
            typenamesize: GateTypeNameSize {
                gatetype: GateType::INOUT,
                name: "pog1".to_owned(),
                size: 0,
            },
            properties: vec!(),
        }
    );

    assert_eq!(
        gate(Span::new("inout pog2 @ss;")).unwrap().1,
        Gate {
            typenamesize: GateTypeNameSize {
                gatetype: GateType::INOUT,
                name: "pog2".to_owned(),
                size: 0,
            },
            properties: vec!(Property {
                nameindex: PropertyNameIndex {
                    name: "ss".to_owned(),
                    index: None,
                },
                keys: vec!(),
            },),
        }
    );

    assert_eq!(
        gate(Span::new("inout pog2 @dd[ee]  @a  @b;")).unwrap().1,
        Gate {
            typenamesize: GateTypeNameSize {
                gatetype: GateType::INOUT,
                name: "pog2".to_owned(),
                size: 0,
            },
            properties: vec!(
                Property {
                    nameindex: PropertyNameIndex {
                        name: "dd".to_owned(),
                        index: Some("ee".to_owned(),),
                    },
                    keys: vec!(),
                },
                Property {
                    nameindex: PropertyNameIndex {
                        name: "a".to_owned(),
                        index: None,
                    },
                    keys: vec!(),
                },
                Property {
                    nameindex: PropertyNameIndex {
                        name: "b".to_owned(),
                        index: None,
                    },
                    keys: vec!(),
                },
            )
        }
    );

    assert!(gate(Span::new("inout pog2 @dd[ee] @a[ff] @b @ccc[hh];")).is_ok());
    assert!(gate(Span::new("inout pog2 @pp(ee=1);")).is_ok());
    assert!(gate(Span::new(
        "inout pog2 @gg(ii=er) @hh(uu=oo) @vv[keke](tt=uw; jkl=23);"
    ))
    .is_ok());
    assert!(gate(Span::new(
        "inout pog2[35] @gg(ii=er) @hh(uu=oo,ii; zz=yy,qq) @vv[keke](tt=uw; jkl=23,45,67);"
    ))
    .is_ok());

}

#[test]
fn parse_gatetype() {
    assert_eq!(gatetype(Span::new("input")).unwrap().1, GateType::INPUT);
    assert_eq!(gatetype(Span::new("output")).unwrap().1, GateType::OUTPUT);
    assert_eq!(gatetype(Span::new("inout")).unwrap().1, GateType::INOUT);
    assert!(gatetype(Span::new(" inout ")).is_err());
    assert!(gatetype(Span::new("imput")).is_err());
}

#[test]
fn parse_gate_typenamesize() {
    assert!(gate_typenamesize(Span::new("input asd[]")).is_ok());
    assert!(gate_typenamesize(Span::new("inout pog")).is_ok());
    assert_eq!(
        gate_typenamesize(Span::new("output   hehe [  ]"))
            .unwrap()
            .1,
        GateTypeNameSize {
            gatetype: GateType::OUTPUT,
            name: "hehe".to_owned(),
            size: 0
        }
    );
}
*/

extern crate walkdir;


#[test]
fn test_parse_all_opp() {

    use walkdir::WalkDir;

    use std::env;
    use std::fs;
    use crate::parser::nedfile;

    let mut num_ok = 0;
    let mut num_fail = 0;
    let mut num_error = 0;

    for entry in WalkDir::new(&env::var("INET_ROOT").unwrap())
        .follow_links(true)
        .into_iter()
        .filter_map(|e| e.ok())
    {
        let f_name = entry.path().to_string_lossy().into_owned();
        if (&f_name).ends_with(".ned") {
            let foo = fs::read_to_string(&f_name);

            println!("{}", f_name);
            match &foo {
                Err(e) => {
                    print!("{} ... ", &f_name);
                    num_error += 1;
                    println!("ERROR\n{:#?}", e);
                    continue;
                }
                Ok(f) => {
                    let foo = f;
                }
            };

            let foo = foo.unwrap();

            let tokenvec = all_consuming(tokenizer::lex_tokens)(Span::new(&foo));

            match tokenvec {
                Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e))
                    => println!("tokenizer error:\n{}", convert_error(foo.as_str(), e)),
                Ok(r) => {
                    let ec = tokens::ErrorsCell::new(vec![]);
                    let tokens = tokens::Tokens::new(&r.1, &ec);
                    let r = all_consuming(nedfile)(tokens);

                    match r {
                        Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e))
                            => println!("parser error"),
                                // format!("parser error at token:\n{:#?}\n{:#?}", e.errors[0].0.start, tokens.tok[e.errors[0].0.start])
                                // format!("parser error at token:\n{:#?}\n{:#?}", e.errors[0].0.location_offset(), tokens.tok[e.errors[0].0.location_offset()])

                        Ok(r) => {
                            println!("ok");
                            num_ok += 1;
                        }
                        _ => println!("parser error")
                    }
                }
                _ => println!("unknown tokenizer error")
            }
        }
    }

    println!("ok: {} fail: {} error: {}", num_ok, num_fail, num_error);
}
