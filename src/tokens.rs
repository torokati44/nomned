extern crate strum;
extern crate strum_macros;

use std::{cell::RefCell, ops::{Range, RangeTo, RangeFrom, RangeFull}};
use std::iter::Enumerate;

use nom_greedyerror::GreedyError;
use nom_locate::LocatedSpan;

use nom_allerror::errorcollector::ErrorCollector;

pub type Span<'a> = LocatedSpan<&'a str>;

pub type TokenizeError<'a> = GreedyError<Span<'a>>;
pub type TokenizeResult<'a, T> = IResult<Span<'a>, T, TokenizeError<'a>>;

use strum_macros::IntoStaticStr;


use nom::{InputLength, InputTake, InputIter, Slice, Compare, Offset, CompareResult, IResult};

#[derive(PartialEq, Debug, Copy, Clone, IntoStaticStr)]
pub enum TokenKind {
    //Illegal,
    EOF, // TODO (for trailing whitespace?)


    // Keywords:
    Import,
    Package,
    Property,
    Module,
    Simple,
    Network,
    Channel,
    ModuleInterface,
    ChannelInterface,
    Extends,
    Like,
    Types,
    Parameters,
    Gates,
    Submodules,
    Connections,
    Allowunconnected,

    // Types
    Double,
    Int,
    String,
    Bool,
    Object,
    Xml,
    Volatile,
    Input,
    Output,
    InOut,

    If,
    For,
    RightArrow,
    LeftArrow,
    DoubleArrow,
    To,
    This,
    Default,
    Ask,
    Const,
    Sizeof,
    Index,
    Exists,
    Typename,
    XmlDoc,
    True,
    False,
    Nan,
    Inf,
    Undefined,
    Nullptr,
    Null,

    /*
{L}({L}|{D})*            { countChars(); return NAME; }
{D}+                     { countChars(); return INTCONSTANT; }
0[xX]{X}+                { countChars(); return INTCONSTANT; }
{D}+{E}                  { countChars(); return REALCONSTANT; }
{D}*"."{D}+({E})?        { countChars(); return REALCONSTANT; }
*/

    // TODO
    Name, // (Span<'a>),
    //PropName, // (Span<'a>), // ???
    IntConstant, // (i64),
    RealConstant, // (f64),
    StringConstant, // (Span<'a>),
    CharConstant, // (char),


    PlusPlus,
    DoubleAsterisk,
    Eq,
    Ne,
    Ge,
    Le,
    Spaceship,
    And,
    Or,
    Xor,
    ShiftLeft,
    ShiftRight,
    DoubleColon,

    /*
    //EXPRESSION_SELECTOR = 326,

    COMMONCHAR = 327,
    INVALID_CHAR = 328,
    MATCH = 329,
    UMIN_ = 330,
    NEG_ = 331,
    NOT_ = 332
    */


    DollarSign,
    SemiColon,
    Comma,
    Colon,
    EqualsSign,
    ParenLeft,
    ParenRight,
    SquareLeft,
    SquareRight,
    CurlyLeft,
    CurlyRight,
    Period,
    Questionmark,

    ExclamationPoint,
    At,

    Pipe,
    Ampersand,
    Hashmark,
    Tilde,

    Hat,
    PlusSign,
    MinusSign,
    Asterisk,

    Slash,
    PercentSign,

    PointyLeft,
    PointyRight,

    Invalid

}

impl TokenKind {

    // for suggestions:
    pub fn source_appearance(&self) -> &'static str {
        match self {

            TokenKind::RightArrow => "-->",
            TokenKind::LeftArrow => "<--",
            TokenKind::DoubleArrow => "<-->",

            TokenKind::IntConstant => "42",
            TokenKind::RealConstant => "3.14",
            TokenKind::StringConstant => "\"foobar\"",
            TokenKind::CharConstant => "'E'",


            TokenKind::PlusPlus => "++",
            TokenKind::DoubleAsterisk => "**",
            TokenKind::Eq => "==",
            TokenKind::Ne => "!=",
            TokenKind::Ge => ">=",
            TokenKind::Le => "<=",
            TokenKind::Spaceship => "<>",
            TokenKind::And => "&&",
            TokenKind::Or => "||",
            TokenKind::Xor => "##",
            TokenKind::ShiftLeft => "<<",
            TokenKind::ShiftRight => ">>",
            TokenKind::DoubleColon => "::",
            TokenKind::DollarSign => "$",
            TokenKind::SemiColon => ";",
            TokenKind::Comma => ",",
            TokenKind::Colon => ":",
            TokenKind::EqualsSign => "=",
            TokenKind::ParenLeft => "(",
            TokenKind::ParenRight => ")",
            TokenKind::SquareLeft => "[",
            TokenKind::SquareRight => "]",
            TokenKind::CurlyLeft => "{",
            TokenKind::CurlyRight => "}",
            TokenKind::Period => ".",
            TokenKind::Questionmark => "?",
            TokenKind::ExclamationPoint => "!",
            TokenKind::At => "@",
            TokenKind::Pipe => "|",
            TokenKind::Ampersand => "&",
            TokenKind::Hashmark => "#",
            TokenKind::Tilde => "~",
            TokenKind::Hat => "^",
            TokenKind::PlusSign => "+",
            TokenKind::MinusSign => "-",
            TokenKind::Asterisk => "*",
            TokenKind::Slash => "/",
            TokenKind::PercentSign => "%",
            TokenKind::PointyLeft => "<",
            TokenKind::PointyRight => ">",

            TokenKind::EOF => "<EOF>",
            TokenKind::Invalid => "<INVALID>",

            // the rest could be done simply by .into() but then it would be CamelCase
            TokenKind::Import => "import",
            TokenKind::Package => "package",
            TokenKind::Property => "property",
            TokenKind::Module => "module",
            TokenKind::Simple => "simple",
            TokenKind::Network => "network",
            TokenKind::Channel => "channel",
            TokenKind::ModuleInterface => "moduleinterface",
            TokenKind::ChannelInterface => "channelinterface",
            TokenKind::Extends => "extends",
            TokenKind::Like => "like",
            TokenKind::Types => "types",
            TokenKind::Parameters => "parameters",
            TokenKind::Gates => "gates",
            TokenKind::Submodules => "submodules",
            TokenKind::Connections => "connections",
            TokenKind::Allowunconnected => "allowunconnected",
            TokenKind::Double => "double",
            TokenKind::Int => "if",
            TokenKind::String => "string",
            TokenKind::Bool => "bool",
            TokenKind::Object => "object",
            TokenKind::Xml => "xml",
            TokenKind::Volatile => "volatile",
            TokenKind::Input => "input",
            TokenKind::Output => "output",
            TokenKind::InOut => "inout",
            TokenKind::If => "if",
            TokenKind::For => "for",
            TokenKind::To => "to",
            TokenKind::This => "this",
            TokenKind::Default => "default",
            TokenKind::Ask => "ask",
            TokenKind::Const => "const",
            TokenKind::Sizeof => "sizeof",
            TokenKind::Index => "index",
            TokenKind::Exists => "exists",
            TokenKind::Typename => "typename",
            TokenKind::XmlDoc => "xmldoc",
            TokenKind::True => "true",
            TokenKind::False => "false",
            TokenKind::Nan => "nan",
            TokenKind::Inf => "inf",
            TokenKind::Undefined => "undefined",
            TokenKind::Nullptr => "nullptr",

            TokenKind::Null => "null", // TODO is this an actual keyword or a "marker" kind?

            TokenKind::Name => "<name>"
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Token<'a> {
    pub kind : TokenKind,
    pub content: Span<'a>,
    pub preceding_whitespace: Span<'a>,
}

impl<'a> Token<'a> {
    pub fn make_invalid() -> Token<'a> {
        Token {
            kind: TokenKind::Invalid,
            content: Span::new(""),
            preceding_whitespace: Span::new("")
        }
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum ErrorCategory {
    Char,
    Context,
    Kind
}

#[derive(Clone, PartialEq, Debug)]
pub struct ErrorWithOffset {
    pub category: ErrorCategory,
    pub depth: i32,
    pub line: usize,
    pub column: usize,
    pub length: usize,
    pub note : String
}

pub type ErrorsCell = RefCell<Vec<ErrorWithOffset>>;

#[derive(Clone, Copy, PartialEq, Debug)]
#[repr(C)]
pub struct Tokens<'a> {
    pub tok: &'a [Token<'a>],
    pub start: usize,
    pub end: usize,

    pub errs: &'a ErrorsCell
}

impl<'a> Tokens<'a> {
    pub fn new(vec: &'a Vec<Token<'a>>, errs: &'a ErrorsCell) -> Self {
        Tokens {
            tok: vec.as_slice(),
            start: 0,
            end: vec.len(),
            errs: errs
        }
    }

    pub fn get_content(&self) -> String {
        self.tok.iter().map(|t| t.content.fragment().to_string()).collect::<Vec<String>>().concat()
    }
}

impl<'a> InputLength for Tokens<'a> {
    #[inline]
    fn input_len(&self) -> usize {
        self.tok.len()
    }
}


impl<'a> InputTake for Tokens<'a> {
    #[inline]
    fn take(&self, count: usize) -> Self {
        Tokens {
            tok: &self.tok[0..count],
            start: 0,
            end: count,
            errs: self.errs
        }
    }

    #[inline]
    fn take_split(&self, count: usize) -> (Self, Self) {
        let (prefix, suffix) = self.tok.split_at(count);
        let first = Tokens {
            tok: prefix,
            start: self.start,
            end: self.start + prefix.len(),
            errs: self.errs
        };
        let second = Tokens {
            tok: suffix,
            start: self.start + prefix.len(),
            end: self.start + prefix.len() + suffix.len(),
            errs: self.errs
        };
        (second, first)
    }
}

impl<'a> InputLength for Token<'a> {
    #[inline]
    fn input_len(&self) -> usize {
        1
    }
}

impl<'a> Slice<Range<usize>> for Tokens<'a> {
    #[inline]
    fn slice(&self, range: Range<usize>) -> Self {
        Tokens {
            tok: self.tok.slice(range.clone()),
            start: self.start + range.start,
            end: self.start + range.end,
            errs: self.errs
        }
    }
}

impl<'a> Slice<RangeTo<usize>> for Tokens<'a> {
    #[inline]
    fn slice(&self, range: RangeTo<usize>) -> Self {
        self.slice(0..range.end)
    }
}

impl<'a> Slice<RangeFrom<usize>> for Tokens<'a> {
    #[inline]
    fn slice(&self, range: RangeFrom<usize>) -> Self {
        self.slice(range.start..self.end - self.start)
    }
}

impl<'a> Slice<RangeFull> for Tokens<'a> {
    #[inline]
    fn slice(&self, _: RangeFull) -> Self {
        Tokens {
            tok: self.tok,
            start: self.start,
            end: self.end,
            errs: self.errs
        }
    }
}

impl<'a> InputIter for Tokens<'a> {
    type Item = &'a Token<'a>;
    type Iter = Enumerate<::std::slice::Iter<'a, Token<'a>>>;
    type IterElem = ::std::slice::Iter<'a, Token<'a>>;

    #[inline]
    fn iter_indices(&self) -> Enumerate<::std::slice::Iter<'a, Token<'a>>> {
        self.tok.iter().enumerate()
    }
    #[inline]
    fn iter_elements(&self) -> ::std::slice::Iter<'a, Token<'a>> {
        self.tok.iter()
    }
    #[inline]
    fn position<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(Self::Item) -> bool,
    {
        self.tok.iter().position(|b| predicate((&b).clone()))
    }
    #[inline]
    fn slice_index(&self, count: usize) -> Option<usize> {
        if self.tok.len() >= count {
            Some(count)
        } else {
            None
        }
    }
}

impl<'a> Compare<TokenKind> for Tokens<'a>
{
  /// compares self to another value for equality
  fn compare(&self, t: TokenKind) -> CompareResult {

    if self.tok.is_empty() {
        CompareResult::Incomplete
    } else {
        if self.tok[0].kind == t {
            CompareResult::Ok
        }
        else
        {
            CompareResult::Error
        }
    }
  }
  /// compares self to another value for equality
  /// independently of the case.
  ///
  /// warning: for `&str`, the comparison is done
  /// by lowercasing both strings and comparing
  /// the result. This is a temporary solution until
  /// a better one appears
  fn compare_no_case(&self, t: TokenKind) -> CompareResult {
    self.compare(t)
  }
}

impl<'a> Offset for Tokens<'a> {

    fn offset(&self, second: &Self) -> usize {
        let fst = self.start;
        let snd = second.start;

        snd as usize - fst as usize
    }
}


impl<'a> ErrorCollector for Tokens<'a> {

    fn collect_from_char(&self, depth: i32, c: char) {
        let cont = &self.tok[0].content;
        let line = cont.location_line();
        let column = cont.get_utf8_column();
        self.errs.borrow_mut().push(ErrorWithOffset {category: ErrorCategory::Char, depth: depth,
            line: line as usize, column: column, length: cont.fragment().len(), note: c.to_string()});
    }

    fn collect_context(&self, depth: i32,  ctx: &'static str) {
        let cont = &self.tok[0].content;
        let line = cont.location_line();
        let column = cont.get_utf8_column();
        self.errs.borrow_mut().push(ErrorWithOffset {category: ErrorCategory::Context, depth: depth,
            line: line as usize, column: column, length: cont.fragment().len(), note: ctx.to_string()});
    }

    fn collect_from_error_kind(&self, depth: i32,  kind: nom::error::ErrorKind) {
        let cont = &self.tok[0].content;
        let line = cont.location_line();
        let column = cont.get_utf8_column();
        self.errs.borrow_mut().push(ErrorWithOffset {category: ErrorCategory::Kind, depth: depth,
            line: line as usize, column: column, length: cont.fragment().len(), note: format!("{:#?}", kind)});
    }
}
