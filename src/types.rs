use crate::tokens::Tokens;

#[derive(Clone, Debug, PartialEq, Default)]
pub struct SourceRange {
    pub from_line : usize,
    pub from_column : usize,
    pub to_line : usize,
    pub to_column : usize
}

impl SourceRange {
    pub fn new(l1:usize, c1:usize, l2:usize, c2:usize) -> SourceRange {
        SourceRange {
            from_line: l1,
            from_column: c1,
            to_line: l2,
            to_column: c2
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum GateType {
    INPUT,
    OUTPUT,
    INOUT,
    UNSPEC,
}

#[derive(Clone, Debug, PartialEq)]
pub struct GateTypeNameSize {
    pub gatetype: GateType,
    pub name: String,
    pub size: i32, // TODO: expression - or vector?
}


#[derive(Clone, Debug, PartialEq)]
pub struct Vector {
    pub expression: Expression,
}

#[derive(Clone, Debug, PartialEq)]
pub struct PropertyKey {
    pub key: Option<String>,
    pub values: Vec<String>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct PropertyNameIndex {
    pub name: String,
    pub index: Option<String>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Property {
    pub nameindex: PropertyNameIndex,
    pub keys: Vec<PropertyKey>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Gate {
    pub typenamesize: GateTypeNameSize,
    pub properties: Vec<Property>,
    pub source_range: SourceRange
}

#[derive(Clone, Debug, PartialEq)]
pub struct Inheritance {
    pub extends: Option<String>,
    pub like: Vec<String>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct InterfaceInheritance {
    pub extends: Vec<String>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ModuleHeader {
    pub kind: String, // "simple" or "module" or "network"
    pub name: String,
    pub inheritance: Inheritance,
    pub source_range: SourceRange
}

#[derive(Clone, Debug, PartialEq)]
pub struct ModuleInterfaceHeader {
    pub name: String,
    pub inheritance: InterfaceInheritance,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ChannelHeader {
    pub name: String,
    pub inheritance: Inheritance,
}

#[derive(Clone, Debug, PartialEq)]
pub struct SimpleModuleDefinition {
    pub header: ModuleHeader,
    pub params: Vec<ParamsItem>,
    pub gates: Vec<Gate>,
    pub source_range : SourceRange
}

#[derive(Clone, Debug, PartialEq)]
pub struct CompoundModuleDefinition {
    pub header: ModuleHeader,
    pub params: Vec<ParamsItem>,
    pub gates: Vec<Gate>,
    pub types: Vec<LocalType>,
    pub submods: Vec<Submodule>,
    pub conns: Vec<ConnectionsItem>,
    pub source_range : SourceRange
}

#[derive(Clone, Debug, PartialEq)]
pub struct ChannelDefinition {
    pub header: ChannelHeader,
    pub params: Vec<ParamsItem>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ModuleInterfaceDefinition {
    pub header: ModuleInterfaceHeader,
    pub params: Vec<ParamsItem>,
    pub gates: Vec<Gate>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum LocalType {
    SimpleModule(SimpleModuleDefinition),
    CompoundModule(CompoundModuleDefinition),
    Channel(ChannelDefinition),
    // TODO many more cases
}

#[derive(Clone, Debug, PartialEq)]
pub struct SubmoduleHeader {
    pub name: String,
    pub vector: Option<Vector>,
    pub moduletype: String,
    pub likeexpr: Option<LikeExpr>,
    pub condition: Option<Condition>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Submodule {
    pub header: SubmoduleHeader,
    pub params: Vec<ParamsItem>,
    pub gates: Vec<Gate>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ChannelSpecHeader {
    pub channelname: Option<String>,
    pub channeltype: Option<String>,
    pub like: Option<String>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ChannelSpec {
    pub header: ChannelSpecHeader,
    pub params: Vec<ParamsItem>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum ConnectionsItem {
    Connection(Connection, Vec<LoopOrCondition>),
    ConnectionGroup(ConnectionGroup),
}

#[derive(Clone, Debug, PartialEq)]
pub struct ConnectionGroup {
    pub loops_and_conditions: Vec<LoopOrCondition>,
    pub connectionsitems: Vec<ConnectionsItem>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Connection {
    // TODO: lots of things, like separating mod and gate parts...
    pub left: String,
    pub channelspec: Option<ChannelSpec>,
    pub right: String,
    pub source_range: SourceRange
}

#[derive(Clone, Debug, PartialEq)]
pub struct Loop {
    pub variable: String,
    pub from_expression: Expression,
    pub to_expression: Expression,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Condition {
    pub expression: Expression,
}

#[derive(Clone, Debug, PartialEq)]
pub enum LoopOrCondition {
    Loop(Loop),
    Condition(Condition),
}

#[derive(Clone, Debug, PartialEq)]
pub enum ParamType {
    DOUBLE,
    INT,
    STRING,
    BOOL,
    XML,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ParamTypeName {
    pub volatile: bool,
    pub paramtype: Option<ParamType>,
    pub name: String,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Expression {
    pub expression: String,
}

#[derive(Clone, Debug, PartialEq)]
pub enum ParamValue {
    Expr(Expression),
    DefExpr(Expression),
    DEFAULT,
    ASK,
}

#[derive(Clone, Debug, PartialEq)]
pub enum LikeExpr {
    Empty,
    Expr(Expression),
    DefExpr(Expression),
}

#[derive(Clone, Debug, PartialEq)]
pub struct ParamTypeNameValue {
    pub typename: ParamTypeName,
    pub properties_left: Vec<Property>,
    pub paramvalue: Option<ParamValue>,
    pub properties_right: Vec<Property>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct PatternValue {
    pub pattern: Pattern,
    pub value: ParamValue,
}

#[derive(Clone, Debug, PartialEq)]
pub enum ParamOrPattern {
    Param(ParamTypeNameValue),
    Pattern(PatternValue),
}

#[derive(Clone, Debug, PartialEq)]
pub enum ParamsItem {
    ParamOrPattern(ParamOrPattern),
    Property(Property),
}

#[derive(Clone, Debug, PartialEq)]
pub struct Pattern {
    pub pattern: String,
}

#[derive(Clone, Debug, PartialEq)]
pub struct PatternIndex<'a> {
    pub from: Option<Tokens<'a>>,
    pub to: Option<Tokens<'a>>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Definition {
    PackageDeclaration(String),
    Import(String),
    FileProperty(Property),
    SimpleModuleDefinition(SimpleModuleDefinition),
    CompoundModuleDefinition(CompoundModuleDefinition),
    ChannelDefinition(ChannelDefinition),
    ModuleInterfaceDefinition(ModuleInterfaceDefinition),
    // TODO more cases
}

#[derive(Clone, Debug, PartialEq)]
pub struct NedFile {
    pub definitions: Vec<Definition>,
}
