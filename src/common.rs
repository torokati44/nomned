extern crate nom;
extern crate nom_greedyerror;
extern crate nom_locate;

use std::str::FromStr;

use nom::{Err as NomErr, IResult, Slice};

use nom::branch::alt;
use nom::bytes::complete::{tag, take_until};
use nom::character::complete::{digit1, anychar, char, multispace1, newline};
use nom::combinator::{cut, map, recognize, value};
use nom::error::ErrorKind;
use nom::multi::fold_many0;
use nom::sequence::{delimited, preceded, terminated, separated_pair};

use nom_greedyerror::{GreedyError, GreedyErrorKind};
use nom_locate::LocatedSpan;
// use nom::error::{ParseError, VerboseError, VerboseErrorKind};

pub type Span<'a> = LocatedSpan<&'a str>;

pub type Error<'a> = GreedyError<Span<'a>>;
pub type ParseResult<'a, T> = IResult<Span<'a>, T, Error<'a>>;

// End-of-input parser.
//
// Yields `()` if the parser is at the end of the input; an error otherwise.
pub fn eoi(i: Span) -> ParseResult<()> {
    if i.fragment().is_empty() {
        Ok((i, ()))
    } else {
        Err(NomErr::Error(Error {
            errors: vec![(i, GreedyErrorKind::Nom(ErrorKind::Eof))],
        }))
    }
}

// A newline parser that accepts:
//
// - A newline.
// - The end of input.
pub fn eol(i: Span) -> ParseResult<()> {
    alt((
        eoi, // this one goes first because it’s very cheap
        value((), newline),
    ))(i)
}

// Apply the `f` parser until `g` succeeds. Both parsers consume the input.
pub fn till<'a, A, B, F, G>(f: F, g: G) -> impl Fn(Span<'a>) -> ParseResult<'a, ()>
where
    F: Fn(Span<'a>) -> ParseResult<'a, A>,
    G: Fn(Span<'a>) -> ParseResult<'a, B>,
{
    move |mut i| loop {
        if let Ok((i2, _)) = g(i) {
            break Ok((i2, ()));
        }

        let (i2, _) = f(i)?;
        i = i2;
    }
}

/// Parse a string until the end of line.
///
/// This parser accepts the multiline annotation (\) to break the string on several lines.
///
/// Discard any leading newline.
pub fn str_till_eol(i: Span) -> ParseResult<Span> {
    map(
        // TODO: \ escape seqs
        recognize(till(alt((value((), tag("\\\n")), value((), anychar))), eol)),
        |i| {
            if i.fragment().as_bytes().last() == Some(&b'\n') {
                i.slice(0..i.fragment().len() - 1)
            } else {
                i
            }
        },
    )(i)
}

/// Parse a single comment.
pub fn comment(i: Span) -> ParseResult<Span> {
    preceded(
        char('/'),
        alt((
            preceded(char('/'), cut(str_till_eol)),
            preceded(char('*'), cut(terminated(take_until("*/"), tag("*/")))),
        )),
    )(i)
}

/// Parse several comments.
pub fn comments(i: Span) -> ParseResult<Span> {
    recognize(many0_(terminated(comment, blank_space)))(i)
}

// A version of many0 that discards the result of the parser, preventing allocating.
pub fn many0_<'a, A, F>(f: F) -> impl Fn(Span<'a>) -> ParseResult<'a, ()>
where
    F: Fn(Span<'a>) -> ParseResult<'a, A>,
{
    move |i| fold_many0(&f, (), |_, _| ())(i)
}
// Blank base parser.
//
// This parser succeeds with multispaces and multiline annotation.
//
// Taylor Swift loves it.
pub fn blank_space(i: Span) -> ParseResult<Span> {
    recognize(many0_(alt((multispace1, tag("\\\n")))))(i)
}

/// In-between token parser (spaces and comments).
///
/// This parser also allows to break a line into two by finishing the line with a backslack ('\').
pub fn blank(i: Span) -> ParseResult<()> {
    value((), preceded(blank_space, comments))(i)
}

pub fn intconstant(input: Span) -> ParseResult<i32> {
    map(
        recognize(digit1),
        |s: Span| -> i32 { FromStr::from_str(s.fragment()).unwrap() },
    )(input)
}

pub fn realconstant(input: Span) -> ParseResult<f64> {
    map(
        recognize(separated_pair(digit1, tag("."), digit1)),
        |s: Span| -> f64 { FromStr::from_str(s.fragment()).unwrap() },
    )(input)
}

// this can't simply return a Span (formerly &str) because it undoes the backslash escaping???
fn in_quotes(buf: Span) -> ParseResult<String> {
    let mut ret = String::new();
    let mut skip_delimiter = false;
    for (i, ch) in buf.fragment().char_indices() {
        if ch == '\\' && !skip_delimiter {
            skip_delimiter = true;
        } else if ch == '"' && !skip_delimiter {
            return Ok((buf.slice(i..), buf.slice(i..i).fragment().to_string()));
        } else {
            ret.push(ch);
            skip_delimiter = false;
        }
    }
    Err(nom::Err::Incomplete(nom::Needed::Unknown))
}

pub fn quoted_string(buf: Span) -> ParseResult<String> {
    delimited(tag("\""), in_quotes, tag("\""))(buf)
}

pub fn string(i: Span) -> ParseResult<String> {
    quoted_string(i)
}
