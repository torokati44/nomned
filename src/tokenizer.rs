extern crate nom;
extern crate nom_greedyerror;
extern crate nom_locate;

use nom::error::ErrorKind;

use nom_greedyerror::GreedyErrorKind;

use crate::common::*;
use crate::*;

use nom::branch::alt;
use nom::bytes::complete::{tag, take_while, take_while_m_n};
use nom::combinator::{map, recognize, not, peek, value, opt};

use nom::multi::{fold_many1, fold_many0};
use nom::sequence::{pair, terminated, tuple};


use tokens::{TokenKind, Token, TokenizeResult};

pub fn name<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    value(
    TokenKind::Name,
    pair(
        take_while_m_n(1, 1, |c: char| c.is_alphabetic() || c == '_'),
        take_while(|c: char| c.is_alphanumeric() || c == '_')
        )
    )(input)
}

pub fn keywords_1<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    alt((
        value(TokenKind::Import, tag("import")),
        value(TokenKind::Package, tag("package")),
        value(TokenKind::Property, tag("property")),
        value(TokenKind::Simple, tag("simple")),
        value(TokenKind::ModuleInterface, tag("moduleinterface")),
        value(TokenKind::Module, tag("module")),
        value(TokenKind::ChannelInterface, tag("channelinterface")),
        value(TokenKind::Channel, tag("channel")),
        value(TokenKind::Network, tag("network")),
        value(TokenKind::Extends, tag("extends")),
        value(TokenKind::Like, tag("like")),
        value(TokenKind::Types, tag("types")),
        value(TokenKind::Parameters, tag("parameters")),
        value(TokenKind::Gates, tag("gates")),
        value(TokenKind::Submodules, tag("submodules")),
        value(TokenKind::Connections, tag("connections")),
        value(TokenKind::Allowunconnected, tag("allowunconnected")),
    ))(input)
}

pub fn keywords_2<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    alt((
        value(TokenKind::Const, tag("const")),
        value(TokenKind::Sizeof, tag("sizeof")),
        value(TokenKind::Index, tag("index")),
        value(TokenKind::Exists, tag("exists")),
        value(TokenKind::Typename, tag("typename")),
        value(TokenKind::XmlDoc, tag("xmldoc")),
        value(TokenKind::True, tag("true")),
        value(TokenKind::False, tag("false")),
        value(TokenKind::Nan, tag("nan")),
        value(TokenKind::Inf, tag("inf")),
        value(TokenKind::Undefined, tag("undefined")),
        value(TokenKind::Nullptr, tag("nullptr")),
        value(TokenKind::Null, tag("null"))
    ))(input)
}

pub fn keywords_3<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    alt((
        value(TokenKind::If, tag("if")),
        value(TokenKind::For, tag("for")),
        value(TokenKind::This, tag("this")),
        value(TokenKind::Default, tag("default")),
        value(TokenKind::Ask, tag("ask"))
    ))(input)
}


pub fn keywords_types<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    alt((
        value(TokenKind::Double, tag("double")),
        value(TokenKind::Int, tag("int")),
        value(TokenKind::String, tag("string")),
        value(TokenKind::Bool, tag("bool")),
        value(TokenKind::Object, tag("object")),
        value(TokenKind::Xml, tag("xml")),
        value(TokenKind::Volatile, tag("volatile")),
        value(TokenKind::Input, tag("input")),
        value(TokenKind::Output, tag("output")),
        value(TokenKind::InOut, tag("inout")),
    ))(input)
}

pub fn arrows<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    alt((
        value(TokenKind::DoubleArrow, tag("<-->")),
        value(TokenKind::RightArrow, tag("-->")),
        value(TokenKind::LeftArrow, tag("<--")),
    ))(input)
}

pub fn operators<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    alt((
        value(TokenKind::PlusPlus, tag("++")),
        value(TokenKind::DoubleAsterisk, tag("**")),

        value(TokenKind::Eq, tag("==")),

        value(TokenKind::Ne, tag("!=")),
        value(TokenKind::Ge, tag(">=")),
        value(TokenKind::Le, tag("<=")),
        value(TokenKind::Spaceship, tag("<>")), // TODO check
        value(TokenKind::And, tag("&&")),
        value(TokenKind::Or, tag("||")),
        value(TokenKind::Xor, tag("##")), // TODO check
        value(TokenKind::ShiftLeft, tag("<<")),
        value(TokenKind::ShiftRight, tag(">>")),
        value(TokenKind::DoubleColon, tag("::")),
    ))(input)
}

pub fn symbols1<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    alt((
        value(TokenKind::DollarSign, tag("$")),
        value(TokenKind::SemiColon, tag(";")),
        value(TokenKind::Comma, tag(",")),
        value(TokenKind::Colon, tag(":")),
        value(TokenKind::EqualsSign, tag("=")),
        value(TokenKind::ParenLeft, tag("(")),
        value(TokenKind::ParenRight, tag(")")),
        value(TokenKind::SquareLeft, tag("[")),
        value(TokenKind::SquareRight, tag("]")),
        value(TokenKind::CurlyLeft, tag("{")),
        value(TokenKind::CurlyRight, tag("}")),
        value(TokenKind::To, tag("..")),
        value(TokenKind::Period, tag(".")),
        value(TokenKind::Questionmark, tag("?")),
    ))(input)
}

pub fn symbols2<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    alt((
        value(TokenKind::ExclamationPoint, tag("!")),
        value(TokenKind::Pipe, tag("|")),
        value(TokenKind::Ampersand, tag("&")),
        value(TokenKind::Hashmark, tag("#")),
        value(TokenKind::Tilde, tag("~")),
        value(TokenKind::Hat, tag("^")),
        value(TokenKind::PlusSign, tag("+")),
        value(TokenKind::MinusSign, tag("-")),
        value(TokenKind::Asterisk, tag("*")),
        value(TokenKind::Slash, tag("/")),
        value(TokenKind::PercentSign, tag("%")),
        value(TokenKind::PointyLeft, tag("<")),
        value(TokenKind::PointyRight, tag(">")),
    ))(input)
}


pub fn keyword<'a>(input: Span<'a>) -> TokenizeResult<TokenKind> {
    terminated(
        alt((
            keywords_1,
            keywords_2,
            keywords_3,
            keywords_types
        )),
        not(peek(take_while_m_n(1, 1, |c: char| c.is_alphanumeric() || c == '_')))
    )(input)
}

pub fn token<'a>(input: Span<'a>) -> TokenizeResult<Token<'a>> {

    let intconst =  value(TokenKind::IntConstant, intconstant);
    let realconst =  value(TokenKind::RealConstant, realconstant);
    let strconst =  value(TokenKind::StringConstant, string);

    let tk = alt(( // TODO: check order
        keyword,
        //propname,
        operators,
        arrows,
        symbols1,
        symbols2,


        // value(TokenKind::At, tag("@")), // NOPE

        name,

        realconst,
        intconst,
        strconst
    ));

    make_ws_and_tok(tk)(input)
}


pub fn make_ws_and_tok<'a>(tok_parser: impl Fn(Span<'a>) -> TokenizeResult<TokenKind>) -> impl Fn(Span<'a>) -> TokenizeResult<'a, Token<'a>> {
    move |i| {
        let (i, ws_rec)  = recognize(&blank)(i)?;
        //let (i, pos) = position(i)?;
        let (_, tok_rec)  = recognize(&tok_parser)(i)?;

        let mapper = |k| Token { /* offset: pos.location_offset(), */ preceding_whitespace : ws_rec, content: tok_rec, kind: k};
        let x = map(
            &tok_parser, mapper);
        x(i)
    }
}


use nom::{Err as NomErr};



pub fn default_context_tokens<'a>(input: Span<'a>) -> TokenizeResult<Vec<Token<'a>>> {
    fold_many1(token, Vec::new(),
        |mut f, g| { f.push(g); f }
    )(input)
}

pub fn lparen<'a>(input: Span<'a>) -> TokenizeResult<Token<'a>> {
    make_ws_and_tok(value(TokenKind::ParenLeft, tag("(")))(input)
}

pub fn rparen<'a>(input: Span<'a>) -> TokenizeResult<Token<'a>> {
    make_ws_and_tok(value(TokenKind::ParenRight, tag(")")))(input)
}

pub fn name_prop<'a>(input: Span<'a>) -> TokenizeResult<Token<'a>> {
    make_ws_and_tok(name)(input)
}

pub fn keyvalues<'a>(input: Span<'a>) -> TokenizeResult<Vec<Token<'a>>> {

    fold_many0(
        alt((
            map(tuple((lparen, keyvalues, rparen)), |(l, mut kv, ri)| { kv.insert(0, l); kv.push(ri); kv }),
            map(name_prop, |t| vec![t])
        )),
        Vec::new(),
        |mut f, mut g| { f.append(&mut g); f }
    )(input)
}

// the point is to exclude parsing keywords inside properties
pub fn property_context_tokens<'a>(input: Span<'a>) -> TokenizeResult<Vec<Token<'a>>> {

    let at = make_ws_and_tok(value(TokenKind::At, tag("@")));

    let lsquare = make_ws_and_tok(value(TokenKind::SquareLeft, tag("[")));
    let rsquare = make_ws_and_tok(value(TokenKind::SquareRight, tag("]")));


    // TODO: @propname[index](key1=value1;key2;key3=value4,value5);
    map(
        tuple((
            at,
            name_prop,

            // index
            opt(tuple((
                lsquare,
                name_prop,
                rsquare
            ))),

            // index
            opt(tuple((
                lparen,
                keyvalues,
                rparen
            ))),


        )),
        |(a, pn, ind, kv)| {
            let mut r = vec![a, pn];

            let mut ii = ind.map_or(Vec::new(), |(l, i, ri)| vec![l, i, ri]);
            r.append(&mut ii);

            let mut kkvv = kv.map_or(Vec::new(), |(l, mut kv, ri)| { kv.insert(0, l); kv.push(ri); kv });
            r.append(&mut kkvv);

            r
        }
    )(input)


}



pub fn eoi2(i: Span) ->TokenizeResult<TokenKind> {
    if i.fragment().is_empty() {
        Ok((i, TokenKind::EOF)) // NOTE: Replace this with TokenKind::Invalid to get all possible accepted next token tags (at the end of input) as "errors" in the refcell
    } else {
        Err(NomErr::Error(Error {
            errors: vec![(i, GreedyErrorKind::Nom(ErrorKind::Eof))],
        }))
    }
}


pub fn endtoken<'a>(input: Span<'a>) -> TokenizeResult<Token<'a>> {
    make_ws_and_tok(eoi2)(input)
}

pub fn lex_tokens<'a>(input: Span<'a>) -> TokenizeResult<Vec<Token<'a>>> {

    map(pair(
        fold_many0(
            alt((
                default_context_tokens,
                property_context_tokens
            )),
            vec![],
            |mut f, mut g| {f.append(&mut g); f }),
        endtoken
    ),
    |(mut a, b)| { a.push(b); a }
    )(input)
}



#[test]
fn test_tokenizer() {
    println!("{:#?}", token(Span::new(" /*  gjklkg */  // pog \n import")));
}
